lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "gitlab_status/version"

Gem::Specification.new do |spec|
  spec.name          = "gitlab_status"
  spec.version       = GitlabStatus::VERSION
  spec.authors       = ["a"]

  spec.summary       = "Check the status of gitlab.com and show response times"

  spec.metadata["allowed_push_host"] = ""

  spec.files         = [
    "lib/gitlab_status.rb",
    "lib/gitlab_status/version.rb",
    "exe/gitlab_status"
  ]
  
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 10.0"
end
