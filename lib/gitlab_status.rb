require "gitlab_status/version"
require "net/http"

module GitlabStatus
  
  def self.cmd_request_times(url, request_count)
    durations = request_time_repeated(url, request_count)
    
    puts "Successful request: #{durations.count}"
    puts "Failed requests: #{request_count - durations.count}"
    if durations.count > 0 
      puts "Request durations (seconds)"
      puts "Slowest: #{durations.max.round(2)}"
      puts "Fastest: #{durations.min.round(2)}"
      puts "Average: #{avg(durations).round(2)}"
    end
  end

  def self.avg(arr)
    return arr.reduce(:+) / arr.size.to_f
  end

  def self.request_time_repeated(url, request_count)
    durations = []
    request_count.times do 
      begin
        durations << request_time(url)
      rescue Exception => e        
        puts "Request failed:", e.message
      end    
    end  
    return durations
  end

  def self.request_time(url) 
    start = Time.now
    res = Net::HTTP.get_response(URI(url))
    elapsed = Time.now - start
    case res
      when Net::HTTPSuccess then
      when Net::HTTPRedirection then
        return elapsed + request_time(res['location'])
      else
        raise RuntimeError, "http status: " + res.code.to_s 
    end
    return elapsed
  end
end
